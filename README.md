# Aplikacja - test rekrutacyjny #

## Struktura JSONów z testami ##


```
#!json
{  
    "title":"Nazwa testu",
    "description":"Opis testu",
    "questions":[
        {  
            "content":"Treść pytania pierwszego?",
            "answers":[  
                {  
                    "content":"To jest poprawna odpowiedź",
                    "isCorrect":true
                },
                {  
                    "content":"Ta odpowiedź też jest poprawna",
                    "isCorrect":true
                },
                {  
                    "content":"To jest niepoprawna odpowiedź",
                    "isCorrect":false
                }
            ]
        },
        {  
            "content":"Treść pytania drugiego?",
            "answers":[  
                {  
                    "content":"Ta odpowiedź jest poprawna",
                    "isCorrect":true
                },
                {  
                    "content":"Ta odpowiedź jest niepoprawna",
                    "isCorrect":false
                }
            ]
        }
    ]
}
```

## Struktura pliku wyjściowego ##

Tutaj dowolna, ale wszystkie informacje na temat przebiegu testu powinny być umieszczone w pliku wyjściowym.
Najlepiej w formie CSV, może być również JSON.

## Flow aplikacji ##

![interview test gui.png](https://bitbucket.org/repo/KrMRxqB/images/93179924-interview%20test%20gui.png)